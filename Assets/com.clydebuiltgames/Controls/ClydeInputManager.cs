﻿using com.clydebuiltgames.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Controls
{
    public class ClydeInputManager : MonoBehaviour
    {
        /// <summary>
        /// Place in PlayerPrefs to store the users custom input mapping
        /// </summary>
        private const string PP_INPUTMAPPING = "UserInputMap"; 
        /// <summary>
        /// Default Mappings
        /// </summary>
        public InputMapping Map;
        /// <summary>
        /// Users custom mappings
        /// </summary>
        [HideInInspector]        
        public InputMapping UserMap;
        public void Awake()
        {  
            UserMap = CInput.Map = Map.Clone();
            LoadMap();
        } 
        [ContextMenu("Load Defaults")]
        public void LoadDefaults()
        { 
            CInput.Map = UserMap = Map.Clone();
        }  
        public void SaveMap()
        { 
            Persistence.Save(PP_INPUTMAPPING, UserMap);
        }
        [ContextMenu("Load Map")]
        public void LoadMap()
        {
            InputMapping loadedMap = Persistence.Load<InputMapping>(PP_INPUTMAPPING);
            if (loadedMap != null)
            {
                UserMap = CInput.Map = loadedMap;
            }
            else
            {
                UserMap = CInput.Map;
            }

        }
        /// <summary>
        /// Generate a basic WASD Layout for 1 player
        /// </summary>
        [ContextMenu("Create Basic Inputs")]
        public void GenerateSuggestedMap()
        {
            Map = new InputMapping();
            var player0 = new PlayerMap();
            player0.SetAxis("Horizontal", KeyCode.D, KeyCode.A);
            player0.SetAxis("Vertical", KeyCode.W, KeyCode.S);
            player0.SetAxis("Horizontal", KeyCode.RightArrow, KeyCode.LeftArrow);
            player0.SetAxis("Vertical", KeyCode.UpArrow, KeyCode.DownArrow);
            player0.SetButton("Fire1", KeyCode.Q);
            player0.SetButton("Fire2", KeyCode.E);
            player0.SetButton("Fire3", KeyCode.R);
            player0.SetButton("Fire1", KeyCode.LeftShift);
            player0.SetButton("Fire2", KeyCode.LeftControl);
            player0.SetButton("Fire2", KeyCode.LeftCommand);
            player0.SetButton("Fire3", KeyCode.LeftAlt);
            player0.SetButton("Jump", KeyCode.Space);
            Map.PlayerMaps.Add(player0); 
            SaveMap();
        }
    }
    [System.Serializable]
    public class InputMapping
    {
        public List<PlayerMap> PlayerMaps = new List<PlayerMap>();

        internal InputMapping Clone()
        {
            InputMapping mapping = new InputMapping();
            mapping.PlayerMaps = new List<PlayerMap>();
            foreach(var map in PlayerMaps)
            {
                mapping.PlayerMaps.Add(map.Clone());
            }
            return mapping;
        }
    }
    [System.Serializable]
    public class PlayerMap
    {
        /// <summary>
        /// If no inputs are found, fall back to unity's built-in Input system. In general you want this to be false.
        /// </summary>
        public bool FallbackToUnity = false;
        public List<AxisMap> AxisMaps = new List<AxisMap>();
        public List<ButtonMap> ButtonMaps = new List<ButtonMap>();
        public void SetButton(string name, KeyCode button) {
            ButtonMaps.Add(new ButtonMap()
            {
                Key = button,
                Name = name
            });
        }
        public void SetAxis(string name, KeyCode positive, KeyCode negative)
        {
            AxisMaps.Add(new AxisMap()
            {
                Name = name,
                Positive = positive,
                Negative = negative
            });
        }

        internal PlayerMap Clone()
        {
            PlayerMap clone = new PlayerMap();
            clone.AxisMaps = new List<AxisMap>();
            clone.ButtonMaps = new List<ButtonMap>();
            clone.FallbackToUnity = FallbackToUnity;
            foreach (var axisMap in this.AxisMaps)
            {
                clone.AxisMaps.Add(axisMap.Clone());
            }
            foreach (var buttonMap in this.ButtonMaps)
            {
                clone.ButtonMaps.Add(buttonMap.Clone());
            }
            return clone;
        }
    }
    [System.Serializable]
    public class ButtonMap
    {
        public string Name;
        public KeyCode Key;

        internal ButtonMap Clone()
        {
            return new ButtonMap()
            {
                Name = Name,
                Key = Key
            };
        }
    }
    [System.Serializable]
    public class AxisMap
    {
        public string Name;
        public KeyCode Positive, Negative;

        internal AxisMap Clone()
        {
            return new AxisMap()
            {
                Name = Name,
                Positive = Positive,
                Negative = Negative
            };
        }
    }
    /// <summary>
    /// Singleton Class for accessing inputs (replaces Unity's 'Input' class)
    /// </summary>
    public static class CInput
    {
        /// <summary>
        /// The current set of Input Mappings
        /// </summary>
        public static InputMapping Map;
        private static IEnumerable<AxisMap> GetAxisMap(string axis, int Player)
        {
            return Map.PlayerMaps[Player].AxisMaps.Where(x => x.Name == axis);
        }
        private static IEnumerable<ButtonMap> GetbuttonMap(string axis, int Player)
        {
            return Map.PlayerMaps[Player].ButtonMaps.Where(x => x.Name == axis);
        }
        public static int GetAxis(string axis, int Player = 0)
        {            
            foreach(var map in GetAxisMap(axis,Player))
            {
                if (Input.GetKey(map.Negative)) return -1;
                if (Input.GetKey(map.Positive)) return 1;
            }
            if(!Map.PlayerMaps[Player].FallbackToUnity) return 0;
            var unityValue = Input.GetAxis(axis);
            return unityValue > 0 ? 1 : unityValue < 0 ? -1 : 0;
        }
        public static bool GetButton(string button, int Player = 0)
        {
            foreach(var map in GetbuttonMap(button, Player))
            {
                if (Input.GetKey(map.Key)) return true;
            }
            if (!Map.PlayerMaps[Player].FallbackToUnity) return false;
            return Input.GetButton(button);
        }
        public static bool GetButtonDown(string button, int Player = 0)
        {
            foreach (var map in GetbuttonMap(button, Player))
            {
                if (Input.GetKeyDown(map.Key)) return true;
            }
            if (!Map.PlayerMaps[Player].FallbackToUnity) return false;
            return Input.GetButtonDown(button);
        }
        public static bool GetButtonUp(string button, int Player = 0)
        {
            foreach (var map in GetbuttonMap(button, Player))
            {
                if (Input.GetKeyUp(map.Key)) return true;
            }
            if (!Map.PlayerMaps[Player].FallbackToUnity) return false;
            return Input.GetButtonUp(button);
        }

    }
}
