﻿using com.clydebuiltgames.Controls;
using com.clydebuiltgames.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.clydebuiltgames.Controls
{
    public class AxisRebindLine : MonoBehaviour
    {
        // Use the CIC Event so that the Controls folder can just be copied without dependencies
        public ClydeInputController.CIC_String_Event OnName;
        public GameObject Positive, Negative;
        
        internal void SetButton(AxisMap MappedTo, Action<AxisMap, bool> RemapCallback)
        {
            OnName.Invoke(MappedTo.Name);
            SetupRemapButton(MappedTo.Negative, Negative, () => RemapCallback(MappedTo, false));
            SetupRemapButton(MappedTo.Positive, Positive, () => RemapCallback(MappedTo, true));
        }
        private void SetupRemapButton(KeyCode code, GameObject AxisBindButton, Action RemapCallback)
        {
            AxisBindButton.SetActive(true);
            var button = AxisBindButton.GetComponent<Button>();
            var text = AxisBindButton.GetComponentInChildren<Text>();
            text.text = code.ToString();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() =>
            {
                RemapCallback();
            });

        }
    }
}