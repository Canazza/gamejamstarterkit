﻿using com.clydebuiltgames.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Controls
{
    public class ClydeInputController:MonoBehaviour
    {
        private List<Guid> DisableControlList = new List<Guid>();

        //Bunch of Unity Events used in the Controls namespace so you can copy just the Controls folder and it should all work
        [System.Serializable]
        public class CIC_Int_Event : UnityEvent<int> { }
        [System.Serializable]
        public class CIC_String_Event : UnityEvent<string> { }
        [System.Serializable]
        public class CIC_Bool_Event : UnityEvent<bool> { }
        public CIC_Int_Event OnHorizontalChanged, OnVerticalChanged;
        public string HorizontalAxis, VerticalAxis;
        [System.Serializable]
        public class CIC_ButtonPress
        {
            public string Button;
            public CIC_Bool_Event IsPressed;
        } 
        [Tooltip("Single button pressses, ie Jump or Shoot")]
        public List<CIC_ButtonPress> ButtonPresses;
        private void OnEnable()
        {
            DisableControlList = new List<Guid>();
    }
        public void Update()
        {
            if (DisableControlList.Count > 0)
            {
                OnHorizontalChanged.Invoke(0);
                OnVerticalChanged.Invoke(0);
                foreach (var bp in ButtonPresses)
                {
                    bp.IsPressed.Invoke(false);
                }
            }
            else
            {
                OnHorizontalChanged.Invoke(CInput.GetAxis(HorizontalAxis));
                OnVerticalChanged.Invoke(CInput.GetAxis(VerticalAxis));
                foreach (var bp in ButtonPresses)
                {
                    bp.IsPressed.Invoke(CInput.GetButton(bp.Button));
                }
            }
        }
        public void DisableControlsFor(float seconds)
        {
            StartCoroutine(DoDisableControls(seconds));
        }
        private IEnumerator DoDisableControls(float seconds)
        {
            //Each call to disable controls gets its own ID, so we don't get weird overlaps and unintended early restoration of controls.
            Guid id = Guid.NewGuid();
            DisableControlList.Add(id);
            yield return new WaitForSeconds(seconds);
            DisableControlList.Remove(id);
        }
    }
}
