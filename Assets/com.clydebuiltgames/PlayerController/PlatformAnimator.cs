﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.clydebuiltgames.PlayerControl
{
    public class PlatformAnimator : MonoBehaviour
    {
        public string Horizontal = "Horizontal";
        public string Grounded = "Grounded";

        public Animator Animator;
        public PlatformController Controller;

        // Update is called once per frame
        void Update()
        {
            Animator.SetBool(Grounded, Controller.IsGrounded);
            Animator.SetFloat(Horizontal, Controller.Velocity.x);
        }
    }
}