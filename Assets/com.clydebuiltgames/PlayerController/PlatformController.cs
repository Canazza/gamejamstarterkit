﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.clydebuiltgames.PlayerControl
{
    public class PlatformController : MonoBehaviour
    {
        [Tooltip("How far the player moves in 1 second")]
        public float WalkSpeed = 3;
        [Tooltip("Gravity, in Units/Second")]
        public float Gravity = 10;
        [Tooltip("Maximum Fall Speed in Units/Second")]
        public float FallSpeed = 10;
        [Tooltip("Jump Speed in Units/Second")]
        public float JumpSpeed = 8;
        public int HorizontalAxisValue { get; set; }
        public int VerticalAxisValue { get; set; }
        public bool JumpPressed { get; set; }
        public bool DownPressed { get; set; }
        private bool IsDroppingDown = false;

        public Vector2 ColliderSize;
        public Vector2 Velocity;
        public LayerMask SolidAndPlatforms, SolidOnly;
        public bool IsGrounded { get; private set; }
        private void Update()
        {
            Velocity.x = HorizontalAxisValue * WalkSpeed;
            if (JumpPressed && IsGrounded)
            {
                if (DownPressed)
                {
                    StartCoroutine(DoDropDown());
                }
                else
                {
                    StartCoroutine(DoJump());
                }
            }
        }
        private const string MOVINGPLATFORM_TAG = "MovingPlatform";
        private void FixedUpdate()
        {
            if (!IsGrounded)
            {
                Velocity.y -= Gravity * Time.deltaTime;
            }
            else
            {
                Velocity.y = 0;
            }
            Velocity.y = Mathf.Clamp(Velocity.y, -FallSpeed, FallSpeed * 2);
            Velocity.x = Mathf.Clamp(Velocity.x, -WalkSpeed, WalkSpeed);
            this.transform.Translate(Velocity * Time.deltaTime);

            var downResult = Velocity.y <= 0 ? Test(Vector2.down, ColliderSize.y / 2, IsDroppingDown ? SolidOnly : SolidAndPlatforms) : null;
            if (downResult.HasValue)
            {
                if (downResult.Value.collider.tag == MOVINGPLATFORM_TAG)
                {
                    this.transform.SetParent(downResult.Value.transform);

                }
                else
                {
                    this.transform.SetParent(null);
                }
                IsGrounded = true;
            }
            else
            {
                this.transform.SetParent(null);
                IsGrounded = false;
            }
            if (Test(Vector2.up, ColliderSize.y / 2, SolidOnly) != null)
            {
                Velocity.y = 0;
            }
            if (Test(Vector2.right, ColliderSize.x / 2, SolidAndPlatforms) != null)
            {
                Velocity.x = 0;
            }
            if (Test(Vector2.left, ColliderSize.x / 2, SolidAndPlatforms) != null)
            {
                Velocity.x = 0;
            }
        }
        public void KnockBack()
        {
            Velocity.x = -Velocity.x;
            Velocity.y = JumpSpeed;
            IsGrounded = false;
            this.transform.SetParent(null);
        }
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(this.transform.position, ColliderSize);
        }
        RaycastHit2D? Test(Vector2 direction, float ColliderSize, LayerMask ColliderMask)
        {

            var hit = Physics2D.Raycast(this.transform.position, direction, ColliderSize, ColliderMask);
            if (hit.collider != null)
            {
                this.transform.position = hit.point - direction * ColliderSize;
                return hit;
            }
            return null;

        }
        IEnumerator DoJump()
        {
            if (!IsGrounded) yield break;
            IsGrounded = false;
            for (float t = 0; t < 1; t += Time.deltaTime / 0.2f)
            {
                Velocity.y = JumpSpeed;
                if (IsGrounded)
                {
                    yield break;
                }
                if (!JumpPressed)
                {
                    Velocity.y = 0;
                    yield break;
                }
                yield return null;
            }
            Velocity.y = JumpSpeed / 2f;
        }
        IEnumerator DoDropDown()
        {
            IsDroppingDown = true;
            Velocity.y = -JumpSpeed;
            for (float t = 0; t < 1; t += Time.deltaTime)
            {
                yield return null;
                if (IsGrounded)
                {
                    IsDroppingDown = false;
                    yield break;
                }
            }
            IsDroppingDown = false;
        }
    }
}