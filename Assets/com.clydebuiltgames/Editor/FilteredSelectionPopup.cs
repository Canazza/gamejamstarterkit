﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using UnityEditor;
using UnityEngine;

namespace com.clydebuiltgames.EditorScripts
{ 
    public class FilteredSelectionPopup : PopupWindowContent
    {
        /// <summary>
        /// Lets the user filter a selection of objects.
        /// </summary>
        /// <param name="Items">List of objects to filter through</param>
        /// <param name="Selected">The index of the currently selected object</param>
        /// <param name="callback">Passes the selected object as a parameter</param>
        public FilteredSelectionPopup(List<object> Items, int Selected, Action<object> callback)
        {
            this.Items = Items;
            FilteredSelectionPopup.Selected = Selected;
            Scroll = Vector2.zero;
            this.Callback = callback;
        }
        private List<object> Items;
        public static int Selected;
        private string Filter = "";
        private Vector2 Scroll;

        public Action<object> Callback;
        private bool focusFilter = false;
        public override void OnOpen()
        {
            base.OnOpen();
            //Auto focus the filter field when first opened so the user can type straight into the box
            focusFilter = true;
        }
        public override void OnGUI(Rect rect)
        {
            
            EditorGUILayout.BeginVertical();
            GUI.SetNextControlName("FilterField"); //This is how Unity Editor GUI Scripts name fields. Isn't it swell?
            Filter = EditorGUILayout.TextField(Filter);

            if (Items != null)
            {
                //Make a copy of the list with the original indices so we can bugger about with it
                List<KeyValuePair<int, object>> ordered = new List<KeyValuePair<int, object>>();
                for(int i = 0; i < Items.Count(); i++)
                {
                    ordered.Add(new KeyValuePair<int, object>(i, Items[i]));
                }
                //Order the list based on the filter input
                //Filter and ordering works like this:
                //Show only the items that contain the filter (case insensitive)
                //Sort by length difference, ie, if it's the same length it'll be near the start of the list
                //This means if you search for A, then the key A will be at the top, while 'Arrow Down' will be much further down the list
                var sortedAndFiltered = ordered.Where(t => t.Value.ToString().ToLower().Contains(Filter.ToLower())).OrderBy(
                    (t) => t.Value.ToString().Length - Filter.Length
                  ).ToList();
                
                Scroll = EditorGUILayout.BeginScrollView(Scroll);
                EditorGUILayout.BeginVertical();
                //Show the filtered list with buttons to select it
                for (int i = 0; i < sortedAndFiltered.Count(); i++)
                {
                    var item = sortedAndFiltered[i]; 
                    if (GUILayout.Button(item.Value.ToString(), GUILayout.ExpandWidth(true)))
                    { 
                        if(Callback != null) Callback.Invoke(item.Value) ;
                        this.editorWindow.Close();
                    }
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
            if(focusFilter)
            {
                //Auto focus on the filter field
                EditorGUI.FocusTextInControl("FilterField"); 
                focusFilter = false;
            }
        }

    }
}
