﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.clydebuiltgames.Damage
{
    public enum DamageType
    {
        None, Piercing, Crushing, Fire, Electrical, Slashing, Cold, Healing
    }
    /// <summary>
    /// Interface used for things that can be damaged, such as players or enemies
    /// </summary>
    public interface IDamageable
    {
        bool DealDamage(int DamageAmount, GameObject source, DamageType sourceType =  DamageType.None);
    }

    /// <summary>
    /// Interface used for things that can be healed, like the player
    /// </summary>
    public interface IHealable
    {
        bool HealDamage(int HealAmount, GameObject source, DamageType sourceType = DamageType.None);

    } 
    /// <summary>
    /// Interface used for things that can be interacted with and triggered, like spikes
    /// </summary>
    public interface ICollidable
    {
        void CollidedWith(GameObject source);
    }
}