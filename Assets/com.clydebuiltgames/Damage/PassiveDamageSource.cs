﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Damage
{
    public class PassiveDamageSource : MonoBehaviour, ICollidable
    {
        public int DamageToDeal;
        public DamageType DamageType = DamageType.None;
        public void CollidedWith(GameObject source)
        {
            var damageable = source.GetComponent<IDamageable>();
            if (damageable != null) damageable.DealDamage(DamageToDeal, this.gameObject, DamageType);
        }
    }
}
