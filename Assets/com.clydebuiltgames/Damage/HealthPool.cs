﻿using com.clydebuiltgames.Utils;
using System; 
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace com.clydebuiltgames.Damage
{
    public class HealthPool : MonoBehaviour, IDamageable, IHealable
    {
        public int MaxHealth;
        public int InitialHealth;
        private int _health = 0;
        public UnityEvent OnDeath;
        public Vector2Event OnHit;
        public float MercyInvulnerabilityTime = 1;
        public FloatEvent OnHealth01;
        private bool _isDirty = false;
        private bool _isInvulnerable = false;
        [Header("Animation")]
        public Animator animator;
        public string AnimatorHealthFloat = "Health";
        public string AnimatorInvulnerableBool = "Invulnerable";
        public int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = Mathf.Clamp(value, 0, MaxHealth);                
                if (_health <= 0) { DoDeath(); }
                _isDirty = true;
            }
        }
        private void Update()
        {
            if (_isDirty)
            {
                OnHealth01.Invoke(_health / (float)MaxHealth);

                if (animator != null && !string.IsNullOrEmpty(AnimatorHealthFloat))
                    animator.SetFloat(AnimatorHealthFloat, _health / (float)MaxHealth);
                if (animator != null && !string.IsNullOrEmpty(AnimatorInvulnerableBool))
                    animator.SetBool(AnimatorInvulnerableBool, _isInvulnerable);

                _isDirty = false;
            }
        }
        private bool IsDead = false;
        private void OnEnable()
        {
            IsDead = false;
            Health = InitialHealth;
            
        }
        public bool DealDamage(int DamageAmount, GameObject source, DamageType sourceType  = DamageType.None)
        {
            if (IsDead) return false;
            if (_isInvulnerable) return false;
            Health -= DamageAmount;
            StartCoroutine(DoMercyInvulnerability());
            OnHit.Invoke(source.transform.position);
            return true;
        }

        public bool HealDamage(int HealAmount, GameObject source, DamageType sourceType = DamageType.None)
        {
            if (IsDead) return false;
            if (Health >= MaxHealth) return false;
            Health += HealAmount;
            return true;
        }
        private void DoDeath()
        {
            IsDead = true;
            OnDeath.Invoke();
        }
        private IEnumerator DoMercyInvulnerability()
        {
            _isInvulnerable = true;
            _isDirty = true;
            yield return new WaitForSeconds(MercyInvulnerabilityTime);
            _isInvulnerable = false;
            _isDirty = true;
        }
    }
}
