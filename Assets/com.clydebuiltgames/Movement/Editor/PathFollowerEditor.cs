﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


namespace com.clydebuiltgames.Movement
{
    /// <summary>
    /// Simple Scene GUI Editor script for displaying handles and manipulating path values
    /// Contains exactly 0 hacks. Which is weird for a Unity Editor Script
    /// </summary>
    [CustomEditor(typeof(PathFollower))]
    public class PathFollowerEditor : Editor
    {
        private PathFollower Target { get { return (PathFollower)target; } }
        private void OnSceneGUI()
        {
            //All positions are relative to the target's parent.
            var offset = Target.transform.parent != null ? Target.transform.parent.position : Vector3.zero;
            var positions = Target.Positions;
            for (int i = 0; i < positions.Count; i++)
            {
                EditorGUI.BeginChangeCheck();
                var newPosition = Handles.PositionHandle(offset + positions[i],Quaternion.identity);
                if(EditorGUI.EndChangeCheck())
                {
                    positions[i] = newPosition - offset;
                }
                if (i > 0)
                { 
                    if(Handles.Button(offset +(positions[i] + positions[i - 1]) / 2, Quaternion.identity,.2f,.3f, Handles.CircleHandleCap))
                    {
                        Target.Positions.Insert(i, (positions[i] + positions[i - 1]) / 2);
                    }
                }
            }
        }
    }
}
