﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.clydebuiltgames.Movement
{
    public class PathFollower : MovementBase
    {
        public List<Vector3> Positions;
        [Tooltip("Speed in Units per Second")]
        public float MovementSpeed = 1;
        public AnimationCurve Easing;
        public enum LoopType
        {
            Jump,  Join, PingPong
        }
        public bool Repeat = true;
        public LoopType Loop;
        void Start()
        {
            StartCoroutine(DoMovement());
        }
        public List<Vector3>  MovementPositions
        {
            get
            {
                var positions = Positions.ToList();
                switch (Loop)
                {
                    case LoopType.PingPong: 
                        for (var i = Positions.Count - 1; i >= 0 ; i--) positions.Add(Positions[i]);
                        break;
                    case LoopType.Join:
                        positions.Add(positions[0]);
                        break;
                }
                return positions;
            }

        }
        IEnumerator DoMovement()
        {
            var positions = MovementPositions;
            for (int p = 0; p < positions.Count; p ++)
            {
                yield return MoveAtConstantSpeed(this.transform.localPosition, positions[p], MovementSpeed, Easing);
            }
            if(Repeat)
            {
                StartCoroutine(DoMovement());
            }
        }
        
        private void OnDrawGizmos()
        {
            var offset = this.transform.parent ? this.transform.parent.position : Vector3.zero;
            for(int i = 0; i < MovementPositions.Count; i++)
            {                
                var position = MovementPositions[i];
                if(i > 0)
                {
                    Gizmos.DrawLine(offset + MovementPositions[i - 1], offset + MovementPositions[i]);
                }                
            }
        }
    }
}