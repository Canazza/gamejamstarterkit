﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Abilities
{
    /// <summary>
    /// Monitors an ability on a cooldown. Used for things like player abilities or enemy weapons, or even timed AI scripts
    /// </summary>
    public class CooldownAbility : MonoBehaviour
    {
        public float CooldownTime = 1;
        private float TimeLeft = 0;
        public UnityEvent OnFire;
        //Setters can be accesed via UnityEvent classes
        public bool AbilityIsActive { get; set; }
        private void Update()
        {
            if (TimeLeft > 0)
            {
                TimeLeft -= Time.deltaTime;
            }
            if(AbilityIsActive && TimeLeft <= 0)
            {
                TimeLeft = CooldownTime;
                OnFire.Invoke();
            }
        }
    }

}