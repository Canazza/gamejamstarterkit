# Unity Game Jam Starter Kit #

A collection of basic tools to get your started making simple 2D games for Game Jams in Unity.

### Features ###

 * Top Down Controller
 * Platform Controller
 * Rebindable Input Manager
 * Player Damage/Health Model
 * Simple Object Pooling 
 * Save/Load Code
 * Extended Unity Events
 * AudioSync

### Expandable Features ###
The following items have basic support for you to build off of:

 * Damage Types (ie Piercing, Fire, Crushing etc) 
 * Spawning (Items, Enemies, bullets etc)

### Examples ###

#### Platform ###
A simple platform example, with just the controller and animator.

#### Platform 2 ####
Same as Platform but includes Spikes, Health and Pickups

#### TopDown ####
A simple example of a top-down movement controller.

#### TopDown 2 ####
Same as TopDown, but with Spikes, Health and Pickups

#### AudioSync ####

Audiosync is an experiment in multi-track audio, allowing you to change the prominence of different pieces of music to create a dynamic soundtrack.

This example is a work in progress.

## Code Details ##

### Controls ###

#### ClydeInputController ####

Listens to Axis and Button presses and provides output events

#### ClydeInputManager ####

For Mapping keys to Buttons and Axes, replacement for Unity's built-in Input system. Can also fall-back to Unity if inputs not found.

Will Load/Save bindings to PlayerPrefs

#### PlatformAnimator ####

Acts as a bridge between PlatformController and the Unity Animator component

#### PlatformController ###

A simple platform player controller. Does not use Unity's Rigidbody physics system, instead relies on Raycasting for collisions.

#### TopDownController####

Simple Top Down player controller. Does not use Unity's Rigidbody physics system, uses Raycasting for collisions.

### Damage ###

#### ActiveDamageMonitor ####

Name is misleading, it monitors for all collidable items (ie Health Pickups) as well as enemies and spikes.

Uses OverlapCollider to find valid ICollidable objects to interact with. Negates the need to add Rigidbody2D to everything. 

#### HealthPickup ####
 
A basic Pickup item that restores Health. Impliments ICollidable.

#### HealthPool ####

Goes alongside ActiveDamageMonitor on a player object. Controls player HP, Mercy Invulnerability, On Hit and On Death events

#### Interfaces ####

 * IDamageable - Something that can be damaged, ie a Player, or a wooden crate
 * IHealable - Something that can be healed, ie a Player
 * ICollidable - Something that can be interacted with by moving over, such as a pressure trigger or damage sources

#### PassiveDamageSource ####

Impliments ICollidable, deals damage to anything that collides with it that implements IDamageable.

### Movement ###

#### MovementBase ####

Abstract class, contains tweening code for moving objects around, relative to its parent

#### PathFollower ####

Basic moving platform that follows a path. Can repeat, PingPong or Loop. Has an Editor Script that gives the edges Handles, and buttons to add new lines in the middle of segments.

### Pooling ###

#### Pool ####

Basic pooling system. Maintains a list of GameObjects, if an object is disabled it will be available for pooling. All this code does is provide a guaranteed object. It does not position or enable objects it provides.

#### Spawner ####

Accesses the Pooling system and positions/enables a prefab.

### Utils ###

#### Persistence ####

Save/Load code using PlayerPrefs and JSON.

#### Reflection ####

Work in progress, but currently contains code to get a component value based on a dot string. ie passing "Transform.localPosition" would return the localPosition.

#### UnityEvents ####

An expanding list of Inspector-ready single-parameter Unity Events that should really be part of the base code by now.